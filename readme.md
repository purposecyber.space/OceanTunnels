# Ocean Tunnels

Program to automatically spin-up a DigitalOcean [VPS](https://en.wikipedia.org/wiki/Virtual_private_server), 
then construct a [Reverse-forwarded SSH Tunnel](https://www.ssh.com/ssh/tunneling/example#sec-Remote-Forwarding) 
for forwarding internet traffic to a server behind an firewall or 
[Floating IP Address](https://en.wikipedia.org/wiki/Network_address_translation). 

## Features
* Supports auto-decomissioning of DigitalOcean Droplet after program ends (to save money).
* Supports connecting a [floating IP address](https://www.digitalocean.com/docs/networking/floating-ips/)
 in DigitalOcean to the Droplet for 
[DNS resolution](https://www.verisign.com/en_US/website-presence/online/how-dns-works/index.xhtml).


### Warnings
#### Charged Services Notice
* By providing this program with your DigitalOcean API key, you are authorizing it to purchase DigitalOcean services on 
your behalf. It is _YOUR_ responsibility to ensure that you understand DigitalOcean's billing policy and that you will 
be responsible for all usage charges incurred. 

#### Internet Security Reminder
* This software is designed to open an internet port back to the calling machine (yours). Do not use this software if 
you do not understand the cyber security implications of doing this! If your host machine is located within an enterprise 
(school, business, etc.) you should obtain the permission of your network administrators to use this software.

## Usage
Download this project to any directory. 
The only real requirement is that [configuration file](#config) be in the current working directory.
Once you have edited config.txt to your satisfaction, simply run
```commandline
./oceantunnels.py
```

<a name="config.txt"></a>

## Configuration
Configuration is accomplished by modifying [config.txt](config.txt) appropriately. The following table summarizes each option and contains a brief description of the field's function. While placing comments into the config.txt file is possible, they will be overwritten upon program execution. 

| Section / Key     | Notes  	|
|:---	        	|:---	|
| **[DigitalOcean]**  |   	  	
| token  	        | <p>API key: See [How to Create a Personal Access Token](https://www.digitalocean.com/docs/api/create-personal-access-token/). </p> <p> You may also set the TOKEN environment variable as a safer way of handeling this sensitive data.	</p><p>Example:<br>`export TOKEN=MY_TOKEN_FROM_DIGITALOCEAN; ./oceantunnels.py`|   	
| droplet_id  	| Optional: Fill in to use an existing droplet. If _auto-destroy-droplet_ is `False`, then the ID of the droplet created will be saved here automatically.  If it is `True`, the droplet with this ID will be __destroyed__.	Use ID number for specific instance, or droplet name to ensure only one droplet with a given name is ever created.|   	
| auto_destroy_droplet| `True` or `False`: Automatically destroys droplet once CTRL+C is pressed.  	   	|
|  **[DropletCreate]** |  [Remove this section to disallow new droplet creation] 	| 	
| name  	        | The name you give your droplet.  	|   	
| region  	    | Choose a region from the [Regions Information API Reference](https://developers.digitalocean.com/documentation/v2/#regions) |   	
| image              | Choose an [Available Snapshot or Distribution Image from the API](https://developers.digitalocean.com/documentation/v2/#sizes)  	|   	
| size 	        | Choose a [Size from the API](https://developers.digitalocean.com/documentation/v2/#sizes) for your    	|   	
| tags 	        | Choose a friendly tag for grouping (or project) purposes.  	|   	
|  **[Forwarding]**   |
| local_port      | Local port number to forward inbound connections to.  |
| remote_interface| `localhost` or `any` :: This parameter will change in the future. For now, recommend using localhost for nginx forwarded connections, external ONLY for PRODUCTION GRADE software, as your machine's listening port will be exposed for the entire world.|
| remote_port     | Remote port number to listen on for inbound connections to forward|
|  **[DNSrecord]** 	|   	
| floating_ip    | Optional: [Floating IPs](https://www.digitalocean.com/docs/networking/floating-ips/) can be set up in the DigitalOcean.com management panel. If there is no floating IP, OceanTunnels will request a new one for you and assign it to the droplet. It will automatically use this on future spinups, so you can point your domain to this ip permanently.	|   	
|  **[SSHKey]** 	    |   	
| key_path       | Filename of an SSH key to validate with to your new server. (RSA only at this time).  	|   	





## Requirements
Requirements can be installed via pip using the command: 

```commandline 
pip3 install "`cat requires.txt`" 
```
#### Primary Reqirements LIst
* paramiko
* python-digitalocean

See [requires.txt](requires.txt) for entire list of requirements