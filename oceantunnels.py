#!/usr/bin/env python3

import configparser
import digitalocean
import paramiko

from os.path import expanduser
from os import environ
from getpass import getuser
from socket import gethostname
from binascii import hexlify
from time import sleep

import logging
import threading
import socket
import select

import signal

# logging setup
logging.basicConfig(format='[%(levelname).1s] %(message)s')
log = logging.getLogger("OceanTunnels")
log.setLevel(logging.DEBUG)

conf = configparser.ConfigParser()

# Gets the SSH id specified in the config and return twin objects:
# One Paramiko key for connecting out, and one Digital Ocean key for receiving on the remote end.
def sshKey_factory(config):
    # Initialize the Paramiko ssh key object, which is the original which must be accepted by Digitalocean
    def load_paramiko_from_file(key_path="~/.ssh/id_rsa", key_type=paramiko.RSAKey):
        return key_type.from_private_key_file(
            expanduser(key_path))

    # Paramiko's .get_fingerprint() is just binary, we need colon separated hex to compare it with
    # Digitalocean's fingerprint format
    def paramiko_to_colon_separated_fingerprint(paramiko_key):
        ssh_fingerprint = hexlify(paramiko_key.get_fingerprint()).decode()
        return ':'.join(a + b for a, b in zip(ssh_fingerprint[::2], ssh_fingerprint[1::2]))

    # does key match existing digitalocean key?
    def check_digitalocean_for_paramiko_key(paramiko_key):
        keys = digitalocean.Manager(token=config["DigitalOcean"]["token"]).get_all_sshkeys()
        paramiko_fingerprint = paramiko_to_colon_separated_fingerprint(paramiko_key)

        for key in keys:
            if paramiko_fingerprint == key.fingerprint:
                return key

        log.info("SSH Key (%s) did not exist on DigitalOcean" % paramiko_fingerprint)

    # Copy a new ssh key to Digitalocean's key storage so it can be assigned to a droplet later
    # check if the key is already loaded into DO first, otherwise you'll get an error if you try to re-upload.
    def copy_paramiko_to_digitalocean(paramiko_key):
        key_name = getuser() + "@" + gethostname(),
        public_key = paramiko_key.get_name() + " " + paramiko_key.get_base64()

        digitalocean_key = \
            digitalocean.SSHKey(token=config["DigitalOcean"]["token"], name=key_name, public_key=public_key)
        digitalocean_key.create()

        log.info("Paramiko key (%s) uploaded to DigitalOcean (%d)" % (
            paramiko_to_colon_separated_fingerprint(paramiko_key),
            digitalocean_key.id))
        return digitalocean_key

    ### Factory code begins here. ###

    # Step 1. Get user's key
    paramiko_key = load_paramiko_from_file(**config["SSHKey"])

    # Step 2. Copy key to Digitalocean
    digitalocean_key = \
        check_digitalocean_for_paramiko_key(paramiko_key=paramiko_key)
    if not digitalocean_key:
        digitalocean_key = copy_paramiko_to_digitalocean(paramiko_key=paramiko_key)

    # Step 3. Return both keys for later use
    log.debug("Loaded and connected Paramiko Key <-> DigitalOcean Key ")
    return paramiko_key, digitalocean_key


# Creates a droplet as specified in the config file, first checking to see if one from existing config already exists
# to avoid duplicating resource.
def droplet_factory(config, digitalocean_key):
    def get_droplet_by_name():
        all_droplets = digitalocean.Manager(token=config["DigitalOcean"]["token"])\
            .get_all_droplets()
        for droplet in all_droplets:
            if droplet.name == config["DigitalOcean"].get("droplet_id"):
                return droplet

        return None

    # Get a reference to an Existing Droplet recorded in config, if it already exists
    def get_droplet_by_id():
        try:
            return digitalocean\
                .Manager(token=config["DigitalOcean"]["token"])\
                .get_droplet(config["DigitalOcean"].get("droplet_id"))

        except (digitalocean.Error, KeyError) as e:
            named_droplet = get_droplet_by_name()
            if named_droplet:
                return named_droplet
            else:
                log.warning("ID or Name not retrieved:  %s" % config["DigitalOcean"].get("droplet_id") + str(e))
                return None

    # Creates a droplet with attributes specified in config, sets new id in config and returns instance of droplet
    def get_new_droplet():
        if conf["DropletCreate"]:
            droplet_settings = dict(conf["DropletCreate"])
            droplet_settings["tags"] = conf["DropletCreate"]["tags"].split(",")
            droplet_settings["ssh_keys"] = [digitalocean_key]
            new_droplet = digitalocean.Droplet(token=config["DigitalOcean"]["token"],
                                               **droplet_settings)
            new_droplet.create()
            log.info("Created new Droplet: %d" % new_droplet.id)
            conf["DigitalOcean"]["droplet_id"] = str(new_droplet.id)
            return new_droplet
        else:
            raise RuntimeError(
                "Droplet could not be located, and [DropletCreate] section is empty or missing in config.")

    # Spins until droplet setup is completed
    def get_droplet_after_setup_completion():
        log.debug("Got droplet, waiting for ready status")
        actions = droplet.get_actions()
        status = ""
        while status != "completed":
            for action in actions:
                action.load()
                # Once it shows complete, droplet is up and running
                if status != action.status:
                    log.debug("Droplet Status: %s" % action.status)
                    status = action.status
            sleep(3)
        # only once setup is complete does the droplet have important fields populated, such as networking information
        return get_droplet_by_id()

    # Attaches static IP to droplet
    def connect_static_ip_to_endpoint():
        manager = digitalocean.Manager(token=config["DigitalOcean"]["token"])

        if config["DNSrecord"].get("Floating_IP"):
            ip = manager.get_floating_ip(config["DNSrecord"]["Floating_IP"])
            if ip.droplet:
                if ip.droplet["id"] != droplet.id:
                    raise RuntimeError(
                        "Could not assign floating IP, already assigned to another droplet (%d)" % ip.droplet["id"])
                else:
                    log.info("Floating IP was already set to correct droplet")
                    return str(ip)
            ip.assign(droplet.id)

        else:
            log.info("No Floating_IP in config, getting a new one.")
            ip = digitalocean.FloatingIP(token=config["DigitalOcean"]["token"], droplet_id=droplet.id)
            ip.create()
            ip.load()
            config["DNSrecord"]["Floating_IP"] = str(ip)
            log.info("Created floating_ip: %s", str(ip))

        # poll to wait for completion of action
        while not ip.droplet:
            sleep(3)
            ip = ip.load()

        return str(ip)

    ### Factory code ###
    droplet = get_droplet_by_id()
    droplet = get_new_droplet() if not droplet else droplet
    droplet = get_droplet_after_setup_completion()

    # todo: setup nginx and letsencrypt and move this to the new "setup_frontend" section
    # assign load balancer to droplet
    static_ip = connect_static_ip_to_endpoint()

    log.info("Connected tunnel endpoint to %s" % static_ip)
    return droplet, static_ip


def reverse_forward_tunnel(remote_port, remote_ip, local_port, transport):
    """ derived from paramiko rforward demo """
    ## Request that the remote SSH server forward a local port to us via a channel
    transport.request_port_forward(remote_ip, remote_port)

    # Messaging mechanism for CTRL+C pressed
    termination_signal_receieved = threading.Event()
    termination_signal_receieved.clear()

    # handle a ctrl + c to tear everything down
    def signal_handler(sig, frame):
        log.info('Ctrl+C pressed. Cleaning up.')
        termination_signal_receieved.set()

    # register the CTRL+C handler
    signal.signal(signal.SIGINT, signal_handler)

    # Agent to listen for connection on remote side via the channel object
    def channel_listener():
        while True:
            # blocks until someone tries to connect to socket on remote host, then ssh forwards to us.
            chan = transport.accept(timeout=10)
            if termination_signal_receieved.is_set():
                log.debug("Local listener terminated.")
                if chan: chan.close()
                return

            if not chan : continue

            # Agent to listen for connection on local side via the socket object (and handles traffic for both sides)
            def socket_listener():
                if termination_signal_receieved.is_set():
                    log.debug("Remote listener terminated.")
                    return

                # blocks until connection we make on behalf of remote is received by listening program locally
                # this section is in the threaded portion because someone else could try to connect while this
                # guy is still trying to set up his own connection.
                sock = socket.socket()
                sock.settimeout(10)
                try:
                    sock.connect(("127.0.0.1", local_port))
                except Exception as e:
                    log.error("Forwarding request to %s:%d failed: %r" % ("127.0.0.1", local_port, e))
                    return
                log.debug(
                    "Connected: Tunnel open %r -> %r -> %r"
                    % (chan.origin_addr, chan.getpeername(), ("127.0.0.1", local_port))
                )

                # Main daemon loop: Forward packets from channel to socket and back until someone closes it
                while True:
                    if termination_signal_receieved.is_set():
                        log.debug("Remote listener terminated.")
                        return


                    r, w, x = select.select([sock, chan], [], [])
                    if sock in r:
                        data = sock.recv(1024)
                        if len(data) == 0:
                            break
                        chan.send(data)
                    if chan in r:
                        data = chan.recv(1024)
                        if len(data) == 0:
                            break
                        sock.send(data)

                chan.close()
                sock.close()
                log.debug("Closed: Tunnel closed from %r" % (chan.origin_addr,))


            ### Begin socket listener run ###
            socket_listener_thread = threading.Thread(target=socket_listener)
            socket_listener_thread.setDaemon(True)
            socket_listener_thread.start()

    ### Begin channel listener run ###
    channel_listener_thread = threading.Thread(target=channel_listener)
    channel_listener_thread.setDaemon(True)
    channel_listener_thread.start()
    log.info('Reverse forwarding tunnel running.')
    return channel_listener_thread

# basic ssh client : this needs a lot of work.
def ssh_connection_to_remote(config, ip, paramiko_key):
    # connect via ssh to endpoint
    ssh = paramiko.SSHClient()

    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # todo: copy over keys to another profile and run tunnel as non-root user
    ssh.connect(hostname=ip, username="root", pkey=paramiko_key)

    # spawn reverse ssh tunnel
    # todo: this would be better to perhaps make a tiny forwarding script instead of changing host configuration params
    if config["Forwarding"]["remote_interface"].lower() == "any":
        log.warning("Enabling remote interface forwarding on droplet!")
        remote_ip = droplet.ip_address
        channel = ssh.invoke_shell()
        channel.send("echo GatewayPorts yes >> /etc/ssh/sshd_config\n")
        channel.send("service ssh restart\n")

        # wait for restart
        sleep(3)
        channel.close()
        ssh.close()

        ssh.connect(hostname=ip, username="root", pkey=paramiko_key)
        remote_ip = "0.0.0.0"

    else:
        remote_ip = "127.0.0.1"

    remote_port = int(config["Forwarding"]["remote_port"])
    local_port = int(config["Forwarding"]["local_port"])
    
    tunnel_thread = reverse_forward_tunnel(remote_port, remote_ip, local_port, ssh.get_transport())

    # run netstat to verify visually that the tunnel is in place
    channel = ssh.invoke_shell()

    sleep(1)
    log.debug(channel.recv(1024).decode())
    sleep(1)
    channel.send(f"netstat -tunap | grep {remote_port}\r\n")
    sleep(1)
    log.debug(channel.recv(1024).decode())
    channel.close()


    # don't leave this section until the tunnel thread has exited
    log.info("Tunnel started: Make sure your application on local machine is listening on %s."
             " Press Ctrl+C to terminate tunnel and clean up.", local_port)
    tunnel_thread.join()
    ssh.close()
    return "Success"



if __name__ == "__main__":
    conf.read("config.txt")

    #see if digitalocean key is in conf file
    obscure_token = True if not conf["DigitalOcean"]["token"] else False

    #check for digitalocean token in environment variable
    if obscure_token and environ.get("TOKEN", ""):
        conf["DigitalOcean"]["token"] = environ["TOKEN"]


    log.debug("Read settings from %s" % "config.txt")

    # Setup ssh key
    paramiko_key, digitalocean_key = sshKey_factory(conf)
    log.debug("SSH keys set")

    # Get handle to / create droplet
    droplet, static_ip = droplet_factory(conf, digitalocean_key)
    log.debug("droplet ready")

    # Setup tunnel
    try:
        ssh_connection_to_remote(conf, static_ip, paramiko_key)
    except paramiko.ssh_exception.NoValidConnectionsError as e:
        log.warning("First attempt to connect via SSH failed. Retrying in 10 seconds...")
        sleep(10)
        ssh_connection_to_remote(conf, static_ip, paramiko_key)
    except Exception as e:
        log.error("Tunnel connection to endpoint failed: %s", str(e))

    # cleanup actions
    if conf["DigitalOcean"]["auto_destroy_droplet"].lower() == "true":
        # if name matc
        if conf["DigitalOcean"]["droplet_id"] != droplet.name:
            conf["DigitalOcean"]["droplet_id"] = ""
        droplet.destroy()
        log.info("Droplet destroyed")

    if obscure_token:
        conf["DigitalOcean"]["token"] = ""

    with open("config.txt", "w") as conf_file:
        conf.write(conf_file)
    log.debug("Config written")